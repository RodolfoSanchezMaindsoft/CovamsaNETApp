import React, { useRef } from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import {  CheckBox } from "@rneui/themed";
import Ionicons from "@expo/vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../hooks/useForm";
import { useContext, useEffect, useState } from "react";
import { ApiContext } from "../context/ApiContext";
import { savitarApi } from "../api/savitarApi";
import SelectDropdown from "react-native-select-dropdown";
import AsyncStorage from "@react-native-async-storage/async-storage";

const ListFooterComponent = () => (
  <Text
    style={{
      fontSize: 16,
      fontWeight: 'bold',
      textAlign: 'center',
      padding: 5,
    }}
  >
    Loading...
  </Text>
);

export default function ListadoProducto() {
  const [productos, setProductos] = useState([]);
  const [id_cliente, setID_Cliente] = useState("");
  const [prodInfo, setProdInfo] = useState([]);
  const [pu, setPU] = useState(0);
  const [cant, setCant] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [loadingMore, setLoadingMore] = useState(false);

  let stopFetch = true;
  var subtotal = 0;
  var inicio = 1;
  var fin = 10;
  const cantidadProductos = 10;

  const { api } = useContext(ApiContext);
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    initProductos();
  }, []); //onInit

  useEffect(() => {
    calcSubtotal();
  }, [pu]);
  useEffect(() => {
    calcSubtotal();
  }, [cant]);

  const initProductos = async () => {
    let localID = await AsyncStorage.getItem("id_cliente");
    setID_Cliente(localID);

    let info = {
      ID_PUNTO_VENTA: auth.auth.ID_PUNTO_VENTA,
      INICIO: inicio,
      FIN: fin,
      BUSCADOR: null,
      ID_CLIENTE: id_cliente ? id_cliente : null,
    };

    try {
      const res = await savitarApi.post(`/obtenerProductos`, info);

      if (res.data.length > 0) {
        let prod = res.data;
        setProdInfo(prodInfo.concat(prod)); //Aquí se almacena toda la informacion del producto
        let aux = prod.map((x) => {
          return x.DESCRIPCION_MODELO;
        });
        setProductos(productos.concat(aux));
      } else {
        console.log("no llega nada");
      }
    } catch (error) {
      console.log("err" + error);
    }
  };

  const loadMore = async () => {
    setLoadingMore(true);
    if(!stopFetch){
      inicio = fin + 1;
      fin = inicio + cantidadProductos;      
      initProductos();
      stopFetch = true;
      return setLoadingMore(false);
      
    }
    setLoadingMore(false);

    
  };

  const calcSubtotal = async () => {
    subtotal = pu * cant;
    let stringst = subtotal.toString();
    //console.log(stringst);
    try {
      await AsyncStorage.setItem("subtotal", stringst);

      //console.log(asynsubt)
    } catch (error) {}
  };

  const renderItem2 = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View style={styles.container}>
        
        <View style={{ flexDirection: "row" }}>
          <CheckBox
            title=""
            checked={false}
            containerStyle={{
              backgroundColor: "white",
              padding: 5,
              marginLeft: 0,
            }}
            checkedColor={"#F5B102"}
            onPress={() => {}}
          />
          <Text style={{ color: "black" }}>{item.DESCRIPCION_MODELO} {item.ID_MODELO}</Text>
        </View>
      </View>
    );
  };



  return (
    <View style={styles.container}>
      <TextInput placeholder="Buscar" value={{}}></TextInput>
      <FlatList
        data={prodInfo}
        keyExtractor={(item) => item.ID_MODELO}
        renderItem={renderItem}
        onEndReached={loadMore}
        onEndReachedThreshold={0.5}
        onScrollBeginDrag={()=>{
          stopFetch = false;

        }}
        ListFooterComponent={() => loadingMore && <ListFooterComponent />}
        style={{ width: "100%", marginBottom: 50, marginTop: 10, height: 430 }}
      ></FlatList>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    padding: 7,
  },
  label: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 15,
  },
  btnCliente: {
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    margin: 10,
    width: "94%",
    height: 35,
    backgroundColor: "white",
  },
  btnNew: {
    backgroundColor: "#F5B102",
    padding: 2,
    marginLeft: 15,
    width: 70,
    alignItems: "center",
    borderRadius: 10,
  },
  btnProd: {
    backgroundColor: "#F5B102",
    padding: 7,
    width: "80%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    marginTop: 40,
    overflow: "visible",
    elevation: 8, // Android
  },
  btnFinal: {
    position: "absolute",
    bottom: 0,
    backgroundColor: "#F5B102",
    padding: 7,
    width: "98%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1DropdownStyle: { backgroundColor: "#EFEFEF", height: 300 },
  dropdown1RowStyle: {
    backgroundColor: "#EFEFEF",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#EFEFEF",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
});
