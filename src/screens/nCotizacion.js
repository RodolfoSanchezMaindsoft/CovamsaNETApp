import React from "react";
import { StatusBar } from "expo-status-bar";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  TextInput,
  FlatList,
  Modal,
  Alert,
  TouchableWithoutFeedback,
  ActivityIndicator,
  TouchableHighlight,
  ScrollView,
} from "react-native";
import axios from "axios";
import { Header, CheckBox } from "@rneui/themed";
import Ionicons from "@expo/vector-icons/Ionicons";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useSelector, useDispatch } from "react-redux";
import { SearchBar } from "@rneui/themed";
import { useContext, useEffect, useState, useRef } from "react";
import { ApiContext } from "../context/ApiContext";
import { savitarApi, savitarTicket } from "../api/savitarApi";


import Clientes from "./clientes";

export default function Npedido({ navigation }) {
  const [clientes, setClientes] = useState([]);
  const [infoClientes, setInfoClientes] = useState([]);
  const [clientesBuscador, setClientesBuscador] = useState([]);
  const [clientID, setClientID] = useState([]); //array de clientes con su ID
  const [producto, setProducto] = useState([]);
  const [producto2, setProducto2] = useState([]);
  const [id_cliente, setID_Cliente] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [buscador, setBuscador] = useState("");
  const [productos, setProductos] = useState([]);
  const [total, setTotal] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const [modal2Visible, setModal2Visible] = useState(false);
  const [modal3Visible, setModal3Visible] = useState(false);
  const [actualIndex, setActualIndex] = useState(0);

  //Constantes de el listado de productos
  const [productosList, setProductosList] = useState([]);
  const [prodInfo, setProdInfo] = useState([]);
  const [pu, setPU] = useState([{ precio: 0, index: 0 }]);
  const [cant, setCant] = useState([{ cantidad: 1, index: 0 }]);
  const [nom, setNom] = useState([{ nombre: "", index: 0 }]);
  const [subtotal, setSubtotal] = useState(0);
  const [inicio, setInicio] = useState(1);
  const [fin, setFin] = useState(100);
  const [selectedClient, setSelectedClient] = useState(null);
  const [loading, setLoading] = useState(false);

  let timeout = null;
  let SearchCli = null;

  const cantidadProductos = 100;

  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    setSelectedClient(null);
    setRefreshing(false);
    initClientes();

    console.log(
      "-------------------------------------------------------------"
    );
  }, []);

  useEffect(() => {
    calcSubtotal();
  }, [subtotal]);

  useEffect(() => {
    calcSubtotal();
  }, [cant]);

  useEffect(() => {
    if (buscador == null || buscador == "") {
      setInicio(1);
      setFin(100);
      initProductos();
    } else {
      initProductos(buscador);
      setInicio(1);
      setFin(100);
      //initProductos(buscador);
    }
  }, [buscador]);

  useEffect(() => {
    if (buscador != null || buscador != "") {
      //initProductos(buscador);
    } else {
      initProductos();
    }
  }, [inicio]);

  useEffect(() => {
    setRefreshing(true);
    calcSubtotal();
    setRefreshing(false);
  }, [producto]);

  useEffect(() => {
    setProdInfo([]);
    setProducto([]);
    setPU([{ precio: 0, index: 0 }]);
    setCant([{ cantidad: 1, index: 0 }]);
    setNom([{ nombre: "", index: 0 }]);
  }, [id_cliente]);

  /* useEffect(()=>{
    initProductos();
  },[prodInfo])
  */

  const ListFooterComponent = () => {
    if (loading)
      return (
        <View>
          <TouchableWithoutFeedback>
            <ActivityIndicator color="#07588f"></ActivityIndicator>
          </TouchableWithoutFeedback>
        </View>
      );
  };
  const initClientes = async () => {
    let info = {
      ID_VENDEDOR: auth.auth.NOMINA,
    };

    try {
      const res = await savitarApi.post(`getClients`, { info });

      if (res.data) {
        let datos = res.data;
        setInfoClientes(datos);
        setClientesBuscador(datos);

        let clientes1 = datos.map((x) => x.NOMBRE);
        setClientes(clientes1);

        let clientes2 = datos.map((cli) => {
          let json = { Nombre: cli.NOMBRE, Id: cli.ID_CLIENTE };
          return json;
        });
        setClientID(clientes2);
      }
    } catch (error) {
      console.log("err" + error);
    }
  };

  const initProductos = async (e = null) => {
    setRefreshing(true)
    let info = {
      ID_PUNTO_VENTA: auth.auth.ID_PUNTO_VENTA,
      INICIO: inicio,
      FIN: inicio + cantidadProductos,
      BUSCADOR: e,
      ID_CLIENTE: id_cliente ? id_cliente : null,
    };

    console.log(info);
    setLoading(true);
    savitarApi
      .post(`/obtenerProductos`, info)
      .then((res) => {
        //console.log(res.data);
        if (e === null || e === "") {
          if (inicio != 1) {
            if (res.data.length > 0) {
              let prod = res.data;
              setProdInfo([...prodInfo, ...prod]); //Aquí se almacena toda la informacion del producto
              let aux = prod.map((x) => {
                return x.DESCRIPCION_MODELO;
              });
              setProductos([...productos, ...aux]);
            } else {
              console.log("no llega nada");
            }
          } else {
            if (res.data.length > 0) {
              let prod = res.data;
              setProdInfo([...prod]); //Aquí se almacena toda la informacion del producto
              let aux = prod.map((x) => {
                return x.DESCRIPCION_MODELO;
              });
              setProductos([...aux]);
            } else {
              console.log("no llega nada");
            }
          }
        } else {
          if (res.data.length > 0) {
            let prod = res.data;
            setProdInfo(prod); //Aquí se almacena toda la informacion del producto
          }
        }
      })
      .catch((error) => {
        console.log("err" + error);
      })
      .finally(() => {
        setLoading(false);
        setRefreshing(false)
      });
    //setBuscador(null);
    setFin(inicio + cantidadProductos);
  };

  const calcSubtotal = async () => {
    let precios = pu.map((x) => x.precio);
    let cantidades = cant.map((x) => x.cantidad);
    let suma = 0;

    for (let i = 0; i < precios.length && i < cantidades.length; i++) {
      suma += cantidades[i] * precios[i];
    }
    setSubtotal(suma.toFixed(2));
    //console.log(subtotal)
  };

  const agregarProducto = async () => {
    let data = {};

    if (producto.length === 0) {
      data.id = 1;
      data.ID_VENTA = null;
      data.DESCRIPCION = null;
      data.NO_PARTE = null;
      data.ID_PUNTO_VENTA = null;
      data.CANTIDAD = 1;
      data.PRECIO = 0;
      data.ID_USUARIO = auth.auth.NOMINA;
    } else {
      data.id = producto[producto.length - 1].id + 1;
      data.ID_VENTA = null;
      data.DESCRIPCION = null;
      data.NO_PARTE = null;
      data.ID_PUNTO_VENTA = null;
      data.CANTIDAD = 1;
      data.PRECIO = 0;
      data.ID_USUARIO = auth.auth.NOMINA;
    }

    producto.push(data);
    pu.push({ precio: 0, index: data.id });
    cant.push({ cantidad: 1, index: data.id });
    nom.push({ nombre: "", index: data.id });

    setRefreshing(true);
  };

  const deleteAct = (id) => {
    const filtered4 = producto.filter((x) => x.id != id);
    const filtered1 = pu.filter((x) => x.index != id);
    const filtered2 = cant.filter((x) => x.index != id);
    const filtered3 = nom.filter((x) => x.index != id);

    setProducto(filtered4);
    setCant(filtered2);
    setNom(filtered3);
    setPU(filtered1);
    setRefreshing(true);
    calcSubtotal();
    setRefreshing(false);
  };

  const renderItem = ({ item, index }) => {
    const nombreSelect = nom.find((x) => x.index === item.id);
    const puSelect = pu.find((x) => x.index === item.id);
    const cantSelect = cant.find((x) => x.index === item.id);

    setRefreshing(false);
    return (
      <View>
        <View style={styles.container}>
          <Text>Producto</Text>
          <TouchableOpacity
            style={styles.btnDropdown}
            onPress={() => {
              initProductos(null);
              setModal2Visible(true);
              setActualIndex(index + 1);
            }}
          >
            <Text
              style={{
                padding: 7,
                fontSize: 12,
                marginRight: "40%",
                width: '85%',
                marginRight: 0,
              }}
            >
              {" "}
              {nombreSelect.nombre != ""
                ? nombreSelect.nombre
                : "Seleccione un producto"}
            </Text>
            <Ionicons
              name="chevron-down"
              size={20}
              style={{ marginTop: 5, width: '10%', marginLeft:'5%' }}
            ></Ionicons>
          </TouchableOpacity>

          <Text>Cantidad</Text>
          <View style={{ flexDirection: "row" }}>
            <TextInput
              style={{
                width: '50%',
                margin: 15,
                borderWidth: 1,
                borderColor: "#f0f0f0",
                height: 40,
                textAlign: "center",
              }}
              cursorColor="#feb72b"
              keyboardType="numeric"
              onChangeText={(x) => {
                cant[item.id].cantidad = x;
                item.CANTIDAD = parseInt(x);
                calcSubtotal();
              }}
            >
              {cantSelect ? cantSelect.cantidad : 1}
            </TextInput>
            <View style={{  alignItems: "center", width:'40%' }}>
              <Text>Precio Unitario</Text>

              <Text>${puSelect ? puSelect.precio : 0}</Text>
              <TouchableOpacity
                style={{
                  backgroundColor: "black",
                  height: 30,
                  width: 100,
                  borderRadius: 15,
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 5,
                }}
                onPress={() => {
                  Alert.alert("", "¿Seguro de eliminar el producto?", [
                    {
                      text: "Cancelar",
                      onPress: () => {},
                    },
                    {
                      text: "Aceptar",
                      onPress: () => {
                        deleteAct(item.id);
                      },
                    },
                  ]);
                }}
              >
                <Ionicons
                  style={{}}
                  name="trash-outline"
                  color={"white"}
                  size={20}
                ></Ionicons>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  };

  const loadData = () => {
    setRefreshing(true);
    if (producto.length == 0) {
      setRefreshing(false);
    } else {
      renderItem();
    }
  };

  const finalizarVenta = async () => {
    let venta = {};
    let error = false;

    if (total == 0) {
      error = true;
    }

    err = producto.filter((prod) => prod.DESCRIPCION == null);
    if (err.length > 0) {
      error = true;
    }
    if (id_cliente == null || id_cliente == "") {
      error = true;
    }
    if (!error) {
      venta.ID_CLIENTE = id_cliente;
      venta.ID_USUARIO = auth.auth.NOMINA;
      venta.ID_PUNTO_VENTA = auth.auth.ID_PUNTO_VENTA;
      venta.SUBTOTAL = subtotal;
      venta.IMPORTE = subtotal;

      //console.log(venta);

      try {
        let res = await savitarApi.post(`generarVenta`, venta);
        //console.log(res.data)
        producto.forEach((x) => {
          x.ID_VENTA = res.data[0].ID_VENTA;
          x.ID_USUARIO = auth.auth.NOMINA;
          x.ID_PUNTO_VENTA = res.data[0].ID_PUNTO_VENTA;
        });

        let product = {
          PRODUCTOS: JSON.stringify(producto),
        };

        let data = await savitarApi.post(`insertarProductos`, product);
        //console.log(data.data);
        let data_prod = await axios
          .get(
            `https://covamsanet.com/WS_savitar.asmx/getURLventaPDF?idVenta=${res.data[0].ID_VENTA}&tipo=2`,
            { responseType: "text" }
          )
          .then(createAlert());
      } catch (error) {
        console.log(error);
      }
    } else {
      Alert.alert(
        "Error",
        "No se ha podido generar la cotización, revisa los campos",
        [
          {
            text: "Aceptar",
            onPress: () => {
              console.log("Error");
            },
          },
        ]
      );
    }
  };

  const createAlert = () => {
    Alert.alert("", "Se ha generado la cotización", [
      {
        text: "Aceptar",
        onPress: () => {
          setID_Cliente(null);
          setProducto([]);
          setSelectedClient(null);
          setRefreshing(true);
          setRefreshing(false);
        },
      },
    ]);
  };

  const renderItem2 = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View style={styles.container2}>
        <TouchableOpacity
          onPress={() => {
            pu[actualIndex].precio = item.PRECIO_VENTA;
            nom[actualIndex].nombre = item.DESCRIPCION_MODELO;
            producto[actualIndex - 1].DESCRIPCION = item.DESCRIPCION_MODELO;
            producto[actualIndex - 1].PRECIO = item.PRECIO_VENTA;
            producto[actualIndex - 1].NO_PARTE = item.ID_MODELO;
            setBuscador(null);
            setProdInfo([]);
            setInicio(1);
            setFin(100);
            setModal2Visible(!modal2Visible);
            calcSubtotal();
            setRefreshing(true);
          }}
        >
          <View
            style={{
              flexDirection: "row",
              height: 40,
              justifyContent: "center",
            }}
          >
            <CheckBox
              title=""
              checked={false}
              containerStyle={{
                backgroundColor: "white",
                padding: 5,
                marginLeft: 0,
                width: "10%",
              }}
              checkedColor={"#07588f"}
            />
            <Text
              style={{
                color: "black",
                fontSize: 12,
                marginTop: 5,
                width: "85%",
              }}
            >
              {item.DESCRIPCION_MODELO} {item.ID_MODELO}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const loadMore = async () => {
    setInicio(fin + 1);
  };

  const renderItemCliente = ({ item, index }) => {
    return (
      <View style={styles.container2}>
        <TouchableOpacity
          onPress={async () => {
            //console.log(item);
            setSelectedClient(item.NOMBRE);
            setID_Cliente(item.ID_CLIENTE);
            try {
              AsyncStorage.removeItem("id_cliente");
              AsyncStorage.setItem("id_cliente", item.ID_CLIENTE.toString());
            } catch (error) {
              console.log(error);
            }
            setModal3Visible(!modal3Visible);
            initClientes();
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "black", fontSize: 12, marginTop: 5 }}>
              {item.ID_CLIENTE}-{item.NOMBRE}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const searchFilter = (text) => {
    if (text) {
      const newData = infoClientes.filter((item) => {
        const itemData = item.NOMBRE ? item.NOMBRE : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setClientesBuscador(newData);
    } else {
      setClientesBuscador(infoClientes);
    }
  };

  const closeModal = () => {
    setModalVisible(!modalVisible);
    setRefreshing(false);
  };
  const closeModal2 = () => setModal2Visible(!modal2Visible);
  const closeModal3 = () => setModal3Visible(!modal3Visible);

  const flatList = useRef(null);

  return (
    <View style={styles.window}>
      <Modal animationType="slide" visible={modalVisible} transparent={true}>
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height: 630,
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "#07588f" }}
            containerStyle={{
              backgroundColor: "#07588f",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Agregar Cliente",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />
          <Clientes closeModal={() => closeModal()}></Clientes>
        </View>
      </Modal>
      <Modal animationType="slide" visible={modal2Visible} transparent={true} >
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height:'95%'
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "black" }}
            containerStyle={{
              backgroundColor: "black",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Productos",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal2()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />
          <SearchBar
            lightTheme={true}
            placeholder="Buscar..."
            onChangeText={(event) => {
              clearTimeout(timeout);
              if (event == "" || event == null) {
                setBuscador(null);
                setProdInfo(null);
                //initProductos();
              } else {
                timeout = setTimeout(() => {
                  if (event.length >= 3 || event.length == 0) {
                    setBuscador(event);
                    setProdInfo([]);
                    //initProductos(event);
                  }
                }, 1000);
              }
            }}
            value={SearchCli}
            platform={"android"}
          />
          <FlatList
            data={prodInfo}
            keyExtractor={(item) => item.ID_MODELO}
            renderItem={renderItem2}
            onEndReached={false}
            ListFooterComponent={ListFooterComponent}
            style={{
              width: "100%",
              marginBottom: 100,
              height: 460,
              backgroundColor: "white",
            }}
          ></FlatList>
        </View>
      </Modal>
      <Modal
        animationType="slide"
        visible={modal3Visible}
        transparent={true}
        onRequestClose={() => {
          closeModal();
        }}
        
      >
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height:'95%'
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "black" }}
            containerStyle={{
              backgroundColor: "black",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Clientes",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal3()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />
          <SearchBar
            lightTheme={true}
            placeholder="Buscar..."
            onChangeText={(event) => {
              searchFilter(event);
            }}
            value={SearchCli}
            platform={"android"}
          />
          <FlatList
            data={clientesBuscador}
            keyExtractor={(item) => item.ID_CLIENTE}
            renderItem={renderItemCliente}
            style={{
              width: "100%",
              marginBottom: 100,
              height: 460,
              backgroundColor: "white",
            }}
          ></FlatList>
        </View>
      </Modal>
      <View
        style={
          modalVisible || modal2Visible || modal3Visible
            ? { backgroundColor: "gray", opacity: 0.5, height: '100%' }
            : { hide: "true" }
        }
      >
        <View style={{height:'22%'}}>
          <Text style={styles.label}>Cliente</Text>

          <View style={{ flexDirection: "row", width: "70%" }}>
            <TouchableOpacity
              style={styles.btnCliente}
              onPress={() => {
                setModal3Visible(true);
              }}
            >
              <Text
                style={{
                  padding: 7,
                  fontSize: 12,
                  width: "85%",
                  marginRight: "2%",
                }}
              >
                {selectedClient != null
                  ? selectedClient
                  : "Seleccione el cliente"}
              </Text>
              <Ionicons
                name="chevron-down"
                size={20}
                style={{ marginTop: 5, marginright: 0, width: "10%" }}
              ></Ionicons>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnNew}
              onPress={() => {
                setModalVisible(true);
                setRefreshing(true);
              }}
            >
              <Ionicons name={"add-circle-outline"} size={30} color={"#fff"} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.btnProd}
            onPress={() => agregarProducto()}
          >
            <Text style={{ color: "white" }}>Agregar Producto</Text>
          </TouchableOpacity>
        </View>
        {Dimensions.get("window").height < 700 ? (
          <FlatList
            data={producto}
            keyExtractor={(item) => item.id}
            renderItem={renderItem}
            refreshing={refreshing}
            onRefresh={loadData}
            ref={flatList}
            onContentSizeChange={() => {
              flatList.current.scrollToEnd();
            }}
            style={{
              width: "100%",
              marginTop:15,
              marginBottom: 0,
              height:'50%'
              //height: Dimensions.get("window").height - 310,
            }}
          />
        ) : (
          <FlatList
            data={producto}
            keyExtractor={(item) => item.id}
            renderItem={renderItem}
            refreshing={refreshing}
            onRefresh={initProductos}
            ref={flatList}
            onContentSizeChange={() => {
              flatList.current.scrollToEnd();
            }}
            style={{
              width: "100%",
              marginBottom: 0,
              //height: Dimensions.get("window").height - 275,
              height:'59%'
            }}
          />
        )}
        <TouchableOpacity
          style={subtotal == 0 ? styles.btnDisable : styles.btnFinal}
          refreshing={refreshing}
          onPress={() => {
            Alert.alert("", "¿Seguro de Terminar la cotización?", [
              {
                text: "Cancelar",
                onPress: () => {},
              },
              {
                text: "Aceptar",
                onPress: () => {
                  finalizarVenta();
                },
              },
            ]);
          }}
          disabled={subtotal == 0 ? true : false}
        >
          <Text style={{ color: "white" }}>Finalizar Pedido ${subtotal}</Text>
        </TouchableOpacity>
      </View>

      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  window: {
    height: Dimensions.get("window").height,
    backgroundColor: "white",
  },
  container: {
    backgroundColor: "white",
    margin: 10,
  },
  container2: {
    flex: 1,
    backgroundColor: "white",
    padding: 7,
  },
  label: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 15,
  },
  btnCliente: {
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    marginLeft: 15,
    marginRight: 15,

    height: 40,
  },
  btnDropdown: {
    borderWidth: 1,
    borderColor: "#E3E3E3",
    borderRadius: 10,
    margin: 10,
    width: "94%",
    height: 45,
    backgroundColor: "white",
    flexDirection: "row",
  },

  btnNew: {
    backgroundColor: "#07588f",
    padding: 2,
    marginLeft: 15,
    width: 70,
    alignItems: "center",
    borderRadius: 10,
  },
  btnProd: {
    backgroundColor: "#07588f",
    padding: 7,
    width: "80%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    marginTop: 20,
    marginBottom: 5,
    overflow: "visible",
    elevation: 8, // Android
  },
  btnDisable: {
    backgroundColor: "#07588f",
    opacity: 0.5,
    padding: 7,
    width: "98%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  btnFinal: {
    backgroundColor: "#07588f",
    padding: 7,
    width: "98%",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1DropdownStyle: { backgroundColor: "#EFEFEF" },
  dropdown1RowStyle: {
    backgroundColor: "#EFEFEF",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 12 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#EFEFEF",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
  rowLeft: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    backgroundColor: "#FE4D33",
  },
});
