import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  TouchableOpacity,
  FlatList,
  Modal,
  ActivityIndicator,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { Header, CheckBox } from "@rneui/themed";
import SelectDropdown from "react-native-select-dropdown";
import { SearchBar } from "@rneui/themed";
import Ionicons from "@expo/vector-icons/Ionicons";
import { useSelector, useDispatch } from "react-redux";
import { useForm } from "../hooks/useForm";
import React, { useContext, useEffect, useState } from "react";
import { ApiContext } from "../context/ApiContext";
import { savitarApi } from "../api/savitarApi";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as FileSystem from "expo-file-system";
import * as Sharing from 'expo-sharing';

export default function Historial() {
  const [tipos, setTipos] = useState();
  const [tipo, setTipo] = useState(null);
  const [id_venta, setId_venta] = useState(null);
  const [ventas, setVentas] = useState([]);
  const [clientes, setClientes] = useState([]);
  const [infoclientes, setInfoClientes] = useState([]);
  const [cliente, setCliente] = useState(null);
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(true);
  const [downloadProgress, setDownloadProgress] = useState();
  const [modalVisible, setModalVisible] = useState(false);
  const [clientesBuscador, setClientesBuscador] = useState([]);
  const [selectedClient, setSelectedClient] = useState(null);
  const [id_cliente, setID_Cliente] = useState(null);
 
  let SearchCli = null;

  const downloadPath = FileSystem.documentDirectory 
  var cantidad = 50;
  var inicio = 1;
  var fin = cantidad;

  const auth = useSelector((state) => state.auth);
  const { StorageAccessFramework } = FileSystem;
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 350);
    console.log(
      "-----------------------------------------------------------------------------------------------------------"
    ),
      initVentasTipo();
  }, []);

 

  

  const buscador = () => {
    inicio = 1;
    fin = cantidad;
    initVentas();
    setRefreshing(true);
  };
  const closeModal3 = () => setModalVisible(!modalVisible);

  const renderItemCliente = ({ item, index }) => {
    return (
      <View style={styles.container2}>
        <TouchableOpacity
          onPress={async () => {
            console.log(item);
            setSelectedClient(item.NOMBRE);
            setCliente(item.ID_CLIENTE);
            initClientes2()
            setModalVisible(!modalVisible);
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={{ color: "black", fontSize: 12, marginTop: 5 }}>
              {item.ID_CLIENTE}-{item.NOMBRE}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const initClientes2 = async () => {
    setRefreshing(true)
    let info = {
      ID_VENDEDOR: auth.auth.NOMINA,
    };

    try {
      const res = await savitarApi.post(`getClients`, { info });
      if (res.data) {
        let datos = res.data;
        setInfoClientes(datos);
        setClientesBuscador(datos);
      }
    } catch (error) {
      console.log("err" + error);
    }
    setRefreshing(false)
  };
  const searchFilter = (text) => {

    if (text != null || text != undefined || text != '') {
      const newData = infoclientes.filter((item) => {
        const itemData = item.NOMBRE ? item.NOMBRE : "".toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setClientesBuscador(newData);
    } else {
      setClientesBuscador([]);
    }
  };

  const initVentasTipo = async () => {
    let tv = await savitarApi.post(`obtenerTiposVentas`, {});
    let datos = tv.data;
    let filter = datos.map((x) => {
      return x.TIPO_VENTA;
    });

    setTipos(filter);
    initVentas();
  };

  const initVentas = async (e = null) => {
    setRefreshing(true);

    let buscar = {
      BUSCADOR: "WHERE V.VENDEDOR = " + auth.auth.NOMINA,
      INICIO: inicio,
      FIN: fin,
    };

    if (tipo != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND V.ID_TIPO_VENTA = " + tipo;
    }

    if (id_venta != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND V.ID_VENTA = " + id_venta;
    }

    if (cliente != null) {
      buscar.BUSCADOR = buscar.BUSCADOR + " AND C.ID_CLIENTE = " + cliente;
    }
    console.log(buscar)
    setRefreshing(true)
    await savitarApi.post(`obtenerVentas`, buscar).then((response)=>{
      console.log(response.data);
      setVentas(response.data);
      initClientes2();
    }).finally(()=>{
      setRefreshing(false)
    })
  };

  const load =  () =>{
    initClientes2();
    setSelectedClient(null);
    setCliente(null)
    initVentas()
   
    
  } 

 

  const downloadCallback = downloadProgress => {
    const progress = downloadProgress.totalBytesWritten / downloadProgress.totalBytesExpectedToWrite;
    setDownloadProgress(progress);
};

const downloadFile = async (id) => {
 
  let fileName = id + '.pdf'
  //alert(fileName)
  const downloadResumable = FileSystem.createDownloadResumable(
    'https://covamsanet.com/Docs/pedidos/'+id+'.pdf',
    downloadPath + fileName,
    {},
    downloadCallback
  );

  try {
    const file = await downloadResumable.downloadAsync()
    await Sharing.shareAsync(file.uri).catch((err)=> console.log(err), err)
  } catch (e) {
    console.error('download error:', e);
  }
}

 

  const renderItem = ({ item, index }) => {
    setRefreshing(false);
    return (
      <View style={styles.card}>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>No Venta:</Text>
          <Text>{item.ID_VENTA}</Text>
          <Text style={{ marginLeft: "50%" }}>{item.TIPO_VENTA}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>Vendedor:</Text>
          <Text>{item.NOMBRE_VENDEDOR}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10, maxWidth: 270 }}>
          <Text style={{ fontWeight: "bold" }}>Cliente:</Text>
          <Text>{item.NOMBRE}</Text>
        </View>
        <View style={{ flexDirection: "row", marginBottom: 10 }}>
          <Text style={{ fontWeight: "bold" }}>Status:</Text>
          <Text>{item.ESTATUS}</Text>
        </View>
        <View style={{ flexDirection: "row" }}>
          <Text style={{ fontWeight: "bold", width:'20%' }}>Subtotal:</Text>
          <Text style={{width:'20%'}}>${item.SUBTOTAL}</Text>
          <Text style={{ fontWeight: "bold",marginLeft:'30%', width:'15%' }}>Total:</Text>
          <Text>${item.IMPORTE.toFixed(2)}</Text>
        </View>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginRight: 10,
              backgroundColor: "#07588f",
              padding: 3,
              width: "15%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              overflow: "visible",
            }}
            onPress={async () => {
              downloadFile(item.ID_VENTA)
              
            }}
          >
            <Ionicons name={"share-social-outline"} color={"white"} size={25} />
          </TouchableOpacity>
         {/*} <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",

              backgroundColor: "black",
              padding: 3,
              width: "15%",
              height: 40,
              borderRadius: 20,
              alignSelf: "center",
              overflow: "visible",
              elevation: 8,
            }}
            onPress={() =>
              shareByEmail(item.ID_VENTA)
            }
          >
            <Ionicons name={"mail-open-outline"} color={"#07588f"} size={25} />
          </TouchableOpacity>*/}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator color="#07588f"></ActivityIndicator>
      ) : (
        <>
        <Modal
        animationType="slide"
        visible={modalVisible}
        transparent={true}
        onRequestClose={() => {
          closeModal();
        }}
      >
        <View
          style={{
            width: "90%",
            alignSelf: "center",
            marginTop: 50,
            height: '95%',
          }}
        >
          <Header
            statusBarProps={{ backgroundColor: "black" }}
            containerStyle={{
              backgroundColor: "black",
              justifyContent: "space-around",
            }}
            leftComponent={{
              text: "Clientes",
              style: { color: "#fff", width: 200 },
            }}
            rightComponent={
              <TouchableOpacity onPress={() => closeModal3()}>
                <Ionicons
                  name={"close-circle-outline"}
                  color={"#fff"}
                  size={25}
                />
              </TouchableOpacity>
            }
          />
          <SearchBar
            lightTheme={true}
            placeholder="Buscar..."
            onChangeText={(event) => {
              searchFilter(event);
            }}
            value={SearchCli}
            platform={"android"}
          />
          <FlatList
            data={clientesBuscador}
            keyExtractor={(item) => item.ID_CLIENTE}
            onRefresh={load}
            refreshing={refreshing}
            renderItem={renderItemCliente}
            style={{
              width: "100%",
              marginBottom: 100,
              height: 460,
              backgroundColor: "white",
            }}
          ></FlatList>
        </View>
      </Modal>
      <View
        style={
          modalVisible 
            ? { backgroundColor: "gray", opacity: 0.5, height: '100%' }
            : { hide: "true" }
        }
      ></View>
          <View style={{ flexDirection: "row", marginTop: 15 }}>
            <SelectDropdown
              buttonStyle={styles.tipo}
              data={tipos}
              onSelect={(selectedItem, index) => {
                if (selectedItem == "Venta") {
                  setTipo(1);
                } else {
                  setTipo(2);
                }
              }}
              defaultButtonText="Tipo de Venta"
              buttonTextAfterSelection={(selectedItem, index) => {
                // text represented after item is selected
                // if data array is an array of objects then return selectedItem.property to render after item is selected
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                // text represented for each item in dropdown
                // if data array is an array of objects then return item.property to represent item in dropdown
                return item;
              }}
              buttonTextStyle={styles.dropdown1BtnTxtStyle}
              renderDropdownIcon={(isOpened) => {
                return (
                  <Ionicons
                    name={isOpened ? "chevron-up" : "chevron-down"}
                    color={"#444"}
                    size={18}
                  />
                );
              }}
              dropdownIconPosition={"right"}
              dropdownStyle={styles.dropdown1DropdownStyle}
              rowStyle={styles.dropdown1RowStyle}
              rowTextStyle={styles.dropdown1RowTxtStyle}
              selectedRowStyle={styles.dropdown1SelectedRowStyle}
            />
            <TextInput
              placeholder="N° de Venta"
              style={styles.txtNo}
              keyboardType={"number-pad"}
              onChangeText={x=>{
                if(x == ''){
                  setId_venta(null)
                }else{
                  setId_venta(x)
                }
                
              }}
              value={id_venta}
            ></TextInput>
          </View>
          <Text style={{ fontSize: 15, marginTop: 15, marginLeft: 10 }}>
            Cliente
          </Text>
          <TouchableOpacity
            style={{  width: "95%",
            alignSelf: "center",
            backgroundColor: "white",
            borderColor: "#DEF2F9",
            borderWidth: 1, height: 40, justifyContent:'center' }}
            onPress={() => {
              setModalVisible(true);
            }}
          >
            <View style={{flexDirection:'row'}}>
            <Text
              style={{
                padding: 7,
                fontSize: 12,
                width: '90%',
                marginRight: 0,
              }}
            >
              {selectedClient != null
                ? selectedClient
                : "Seleccione el cliente"}
            </Text>
            <Ionicons
              name="chevron-down"
              size={20}
              style={{ marginTop: 5, marginright: 0, width:'10%' }}
            ></Ionicons>
            </View>
          </TouchableOpacity>
          
          <TouchableOpacity style={styles.btnFinal} onPress={buscador}>
            <Text style={{ color: "white" }}>Buscar</Text>
          </TouchableOpacity>
          <View
            style={
              ventas.length == 0
                ? { hide: "true" }
                : {
                    backgroundColor: "white",
                    marginTop: 20,
                    height: "70%",
                    justifyContent: "center",
                    alignItems: "center",
                  }
            }
          >
            <FlatList
              data={ventas}
              keyExtractor={(item) => item.ID_VENTA}
              renderItem={renderItem}
              refreshing={refreshing}
              onRefresh={load}
              style={{
                width: "98%",
                marginBottom: 35,
                alignSelf: "center",
                backgroundColor: "white",
                height:'100%'
              }}
            ></FlatList>
          </View>
          <View
            style={
              ventas.length == 0 ? { hide: "true" } : { alignItems: "center" }
            }
          >
            <Text style={{ color: "black" }}>No se encontró info</Text>
          </View>
        </>
      )}
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: Dimensions.get("window").height - 1,
    backgroundColor: "#FFFFFF",
  },
  container2: {
    flex: 1,
    backgroundColor: "white",
    padding: 7,
  },
  tipo: {
    backgroundColor: "#FFFFFF",
    marginLeft: 15,
    marginRight: 10,
    width: "45%",
    color: "black",
    borderWidth: 1,
    borderColor: "#DEF2F9",
  },
  btnFinal: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    backgroundColor: "#07588f",
    padding: 7,
    width: "90%",
    height: 40,
    borderRadius: 20,
    alignSelf: "center",
    overflow: "visible",
    elevation: 8, // Android
  },
  txtNo: {
    color: "black",
    borderWidth: 1,
    borderColor: "#DEF2F9",
    fontWeight: "bold",
    fontSize: 14,
    width: "45%",
    padding: 7,
  },
  card: {
    borderWidth: 1.5,
    borderColor: "#DEF2F9",
    margin: 10,
    borderRadius: 10,
    padding: 10,
  },
  dropdown1BtnTxtStyle: { color: "#444", textAlign: "left", fontSize: 15 },
  dropdown1DropdownStyle: { backgroundColor: "#fff" },
  dropdown1RowStyle: {
    backgroundColor: "#fff",
    borderBottomColor: "#C5C5C5",
  },
  dropdown1RowTxtStyle: { color: "#444", textAlign: "left", fontSize: 15 },
  dropdown1SelectedRowStyle: { backgroundColor: "rgba(0,0,0,0.1)" },
  dropdown1searchInputStyleStyle: {
    backgroundColor: "#fff",
    borderRadius: 8,
    borderBottomWidth: 1,
    borderBottomColor: "#444",
  },
});
