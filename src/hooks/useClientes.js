import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { savitarApi } from "../api/savitarApi";

export const useClientes = () => {
  const [selectedClient, setSelectedClient] = useState(null);

  const [clientes, setClientes] = useState([]);
  const [infoClientes, setInfoClientes] = useState([]);
  const [clientesBuscador, setClientesBuscador] = useState([]);
  const [clientID, setClientID] = useState([]); //array de clientes con su ID

  const [refreshing, setRefreshing] = useState(false);
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    setSelectedClient(null);
    setRefreshing(false);
    initClientes();
    console.log(
      "-------------------------------------------------------------"
    );
  }, []);

  const initClientes = async () => {
    let info = {
      ID_VENDEDOR: auth.auth.NOMINA,
    };

    try {
      const res = await savitarApi.post(`getClients`, { info });

      if (res.data) {
        let datos = res.data;
        setInfoClientes(datos);
        setClientesBuscador(datos);

        let clientes1 = datos.map((x) => x.NOMBRE);
        setClientes(clientes1);

        let clientes2 = datos.map((cli) => {
          let json = { Nombre: cli.NOMBRE, Id: cli.ID_CLIENTE };
          return json;
        });
        setClientID(clientes2);
      }
    } catch (error) {
      console.log("err" + error);
    }
  };

  return {
    selectedClient,
    setSelectedClient,
    clientes,
    setClientes,
    infoClientes,
    setInfoClientes,
    clientesBuscador,
    setClientesBuscador,
    clientID,
    setClientID,
    refreshing,
    setRefreshing,
  };
};
