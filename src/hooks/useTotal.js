import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { savitarApi } from "../api/savitarApi";

export const useTotal = (state, action) => {
  const [prodInfo, setProdInfo] = useState([]);
  const [pu, setPU] = useState([{ precio: 0, index: 0 }]);
  const [cant, setCant] = useState([{ cantidad: 1, index: 0 }]);
  const [nom, setNom] = useState([{ nombre: "", index: 0 }]);
  const [producto, setProducto] = useState([]);
  const [id_cliente, setID_Cliente] = useState(null);

  useEffect(() => {
    setProdInfo([]);
    setProducto([]);
    setPU([{ precio: 0, index: 0 }]);
    setCant([{ cantidad: 1, index: 0 }]);
    setNom([{ nombre: "", index: 0 }]);
  }, [id_cliente]);


  return {
    id_cliente, setID_Cliente,
    prodInfo, setProdInfo,
    pu, setPU,
    cant, setCant,
    nom, setNom,
    producto, setProducto,
  };
};
