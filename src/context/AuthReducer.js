export const initialAuthState = {
    user: null,
    loading: false,
    error: {
        flag: false,
        msg: null
    }
}

export const authReducer = (state = initialAuthState, action) => {
    switch(action.type){
        case 'fetch_user':
            return {
                ...state,
                loading: true
            };
        case 'fetch_user_success':
            return {
                ...state,
                user: action.payload,
                loading: false,
                error: {
                    flag: false,
                    msg: null
                }
            };
        case 'user_not_registered':
            return {
                ...state,
                loading: false,
                error: {
                    flag: true,
                    msg: action.payload
                },
                user: null
            };
        case 'user_sign_in':
            return {
                ...state,
                loading: true
            };
        case 'user_sign_in_failed':
            return {
                ...state,
                user: null,
                loading: false,
                error: {
                    flag: true,
                    msg: action.payload
                }
            };
        case 'user_sign_out':
            return initialAuthState;
        default:
            return state;
    }
}