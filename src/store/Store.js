import { applyMiddleware, combineReducers, createStore } from 'redux';

import thunk from 'redux-thunk';

import { AuthReducer as auth } from './reducers/AuthReducer';

const reducers = combineReducers({
    auth
});

let middleware = [thunk];

export default createStore(reducers, {}, applyMiddleware( thunk));