import { useState, useContext, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { ApiContext } from "./src/context/ApiContext";
import AsyncStorage from "@react-native-async-storage/async-storage";
import AuthLoadingScreen from "./src/screens/AuthLoadingScreen";
//import messaging from "@react-native-firebase/messaging";

import { Alert } from "react-native";
import { savitarApi } from "./src/api/savitarApi";

export default function AppCommon({ children }) {
  const { api } = useContext(ApiContext);
  const { signIn } = api;
  const auth = useSelector((state) => state.auth);
  const authStillNotResponded = useRef(false);
  const [load, setLoad] = useState(false);
  const authState = useRef("loading");
  const dispatch = useDispatch();

  const [token, setToken] = useState(null);


  useEffect(() => {
    
    getUser();
    /*if (requestUserPermission()) {
      //return fcm token
      messaging()
        .getToken()
        .then((token) => {
          let info={NOMINA: auth.auth.NOMINA, TOKEN:token}
          savitarApi.post('tokens', info)          
          setToken(token)
          console.log(token);
        });
    } else {
      console.log("Failed to get token", authStatus);
    }

    messaging()
      .getInitialNotification()
      .then(async (remoteMessage) => {
        if (remoteMessage) {
          console.log(
            "Notification caused app to open from quit state:",
            remoteMessage.notification
          );
        }
      });

    messaging().onNotificationOpenedApp(async (remoteMessage) => {
      console.log(
        "Notification caused app to open from background state:",
        remoteMessage.notification
      );
    });

    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log("Message handled in the background!", remoteMessage);
    });

    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      Alert.alert("A new FCM message arrived!", remoteMessage.body);
    });

    return unsubscribe;*/
  }, []);

  /*const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log("Authorization status:", authStatus);
    }
  };*/

  const getUser = async () => {
    try {
      const res = await AsyncStorage.getItem("user");

      if (res) {
        let auxRes = JSON.parse(res);

        dispatch(signIn(auxRes.CORREO, auxRes.PASSWORD));
        setTimeout(() => {
          setLoad(true);
        }, 350);
      } else {
        setTimeout(() => {
          setLoad(true);
        }, 350);
      }
    } catch (e) {
      setLoad(true);
      console.log(e);
    }
  };
  if (!load) {
    return <AuthLoadingScreen />;
  }

  return children;
}
